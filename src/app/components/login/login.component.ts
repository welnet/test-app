import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(
    private router: Router
  ) { }

  username: string;
  password: string;

  login() : void {
    if(this.username == 'mwelenc' && this.password == 'Test1234') {
      this.router.navigate(["user"]);
    } else {
      alert("Niepoprawne dane");
    }
  }

}
